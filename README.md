# Code in the Dark

## Setup

1. `npm i`
2. `npm start`
3. Write some code.

## Rules

- Given: 15 minutes, a screenshot to recreate and the assets needed.
- Only HTML & CSS.
- No peaking at the progress.
